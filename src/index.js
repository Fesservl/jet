import './style.css';
window.onload = function() {
    const information = {
        headers:{
            name: 'Name',
            position: 'Position',
            office: 'Office',
            age: 'Age',
            startDate: 'Start date',
            salary: 'Salary'
        },
        personnel:[
            {
                id:0,
                name: 'Thor Walton',
                position: 'Developer',
                office: 'New York',
                age: 61,
                startDate: '2013/08/01',
                salary: '$98,540'
            },
            {
                id:1,
                name: 'Quinn Flynn',
                position: 'Support Lead',
                office: 'Edinburg',
                age: 22,
                startDate: '2013/03/03',
                salary: '$342,000'
            },
            {
                id:2,
                name: 'Jennifer Acosta',
                position: 'Junior Javascript Developer',
                office: 'Edinburg',
                age: 43,
                startDate: '2013/02/01',
                salary: '$75,650'
            },
            {
                id:3,
                name: 'Haley Kennedy',
                position: 'Senior Marketing Designer',
                office: 'London',
                age: 43,
                startDate: '2012/12/18',
                salary: '$313,500'
            },
            {
                id:4,
                name: 'Brielle Williamson',
                position: 'Integration Specialist',
                office: 'New York',
                age: 61,
                startDate: '2012/12/02',
                salary: '$372,000'
            },
            {
                id:5,
                name: 'Michal Silva',
                position: 'Marketing Designer',
                office: 'London',
                age: 66,
                startDate: '2012/11/27',
                salary: '$198,500'
            },
            {
                id:6,
                name: 'Bradley Greer',
                position: 'Software Engineer',
                office: 'London',
                age: 41,
                startDate: '2012/10/13',
                salary: '$132,000'
            },
            {
                id:7,
                name: 'Dai Rios',
                position: 'Personnel Lead',
                office: 'Edinburg',
                age: 35,
                startDate: '2012/09/26',
                salary: '$217,500'
            },
            {
                id:8,
                name: 'Herror Chandler',
                position: 'Sales Assistant',
                office: 'San Francisco',
                age: 59,
                startDate: '2012/08/06',
                salary: '$137,500'
            }
        ]

    };

    const tab = function() {
        let initialData = {},
            dataForPaginaton = {},
            statePagination = false,
            amountStr,
            rootContainer,
            id = 0;
        const stateSort = {};
        function searchData(value) {
            const keys = Object.keys(initialData.headers);
            const personnel = initialData.personnel.filter((person) => {
                if (keys.some((key)=>{
                    return String(person[key]).toLowerCase().replace( /\s/g, '' ) === value.toLowerCase().replace( /\s/g, '' );
                })) {
                    return person;
                }
            });
            createTable({ ...initialData, personnel});
        }
        function createSearch() {
            const div = document.createElement('div');
            div.id = `search_${id}`;
            div.className = 'search';
            const input = document.createElement('input');
            input.id = `input_${id}`;
            const btn =  document.createElement('button');
            btn.onclick = () => { searchData(document.getElementById(`input_${id}`).value);}
            btn.innerText = 'search';
            div.appendChild(input);
            div.appendChild(btn);
            return div;
        }
        function loadingData(numBtn, amountStr) {
            const start = numBtn * amountStr - amountStr;
            const end = ( numBtn * amountStr  < initialData.personnel.length ) ? numBtn * amountStr  : initialData.personnel.length;
            const personnel = initialData.personnel.slice(start, end);
            dataForPaginaton = {...initialData, personnel};
            createTable(dataForPaginaton);
        }

        function createPagination(amountStr) {
            const amountBtn = Math.ceil(initialData.personnel.length / amountStr);
            const div = document.createElement('div');
            div.id = `pagination_${id}`;
            div.className = 'pagination';
            for (let i = 0; i < amountBtn; i++) {
                const btn =  document.createElement('button');
                btn.innerText = i + 1;
                btn.onclick = ( ) => { loadingData(i + 1, amountStr); };
                div.appendChild(btn);
            }
            return div;
        }

        function saveState(elem) {
            const keys = Object.keys(initialData.headers);
            const editPerson = {};
            Array.prototype.forEach.call(elem.children, (td, i)=>{
                editPerson[keys[i]] = td.innerText;
            });
            const personnel = initialData.personnel.map((person) => {
                if (+person.id === +elem.id) {
                    return {...person, ...editPerson};
                }
                return person;
            });
            initialData = { ...initialData, personnel };
            createTable(initialData);
        }

        function createInputTable(data, parent) {
            const input = document.createElement('input');
            input.type = 'text';
            input.value = data;
            input.onblur = function() { this.outerHTML = this.value; saveState(parent);};
            return input;
        }
        function createCellTable(data) {
            const td = document.createElement('td');
            td.innerText = data;
            td.onclick = function() { if (!this.getElementsByTagName('input')[0]) { this.innerHTML = ''; this.appendChild(createInputTable(data, td.parentNode)).focus();}  };
            return td;
        }

        function createTable(info) {
            const data = info;
            const table = document.createElement('table');
            table.id = `table_${id}`;
            const thead = document.createElement('thead');
            const tr = document.createElement('tr');
            Object.keys(data.headers).forEach((head)=>{
                const th = document.createElement('th');
                th.innerText = data.headers[head];
                th.onclick = () => {sortTable(head);};
                tr.appendChild(th);
            });
            thead.appendChild(tr);
            const tbody = document.createElement('tbody');

            data.personnel.forEach((worker)=>{
                const trBody = document.createElement('tr');
                trBody.id = worker.id;
                trBody.appendChild(createCellTable(worker.name));
                trBody.appendChild(createCellTable(worker.position));
                trBody.appendChild(createCellTable(worker.office));
                trBody.appendChild(createCellTable(worker.age));
                trBody.appendChild(createCellTable(worker.startDate));
                trBody.appendChild(createCellTable(worker.salary));
                tbody.appendChild(trBody);
            });

            table.appendChild(thead);
            table.appendChild(tbody);
            if (document.getElementById(`search_${id}`)) { rootContainer.removeChild((document.getElementById(`search_${id}`)));}
            if (document.getElementById(`table_${id}`)) { rootContainer.removeChild((document.getElementById(`table_${id}`)));}
            if (document.getElementById(`pagination_${id}`)) { rootContainer.removeChild(document.getElementById(`pagination_${id}`));}

            rootContainer.appendChild(createSearch());
            rootContainer.appendChild(table);
            statePagination && rootContainer.appendChild(createPagination( amountStr ));
        }
        function sortTable(caption) {
            const objData = statePagination ? dataForPaginaton : initialData;
            const resultSort = {
                ...objData,
                personnel:objData.personnel.sort((persA, persB)=> stateSort[caption] ? persA[caption] > persB[caption] :  persA[caption] < persB[caption] )
            };
            stateSort[caption] = !stateSort[caption];
            createTable(resultSort);
        }
        function getData(data, PagTool, root) {
            statePagination = PagTool.statePag;
            initialData = data;
            amountStr = PagTool.amountStr;
            rootContainer = root;
            id++;
            if (document.getElementById(`table_${id}`)) { id++;}
            Object.keys(initialData.headers).forEach((cap)=>{
                stateSort[cap] = false;
            });
            statePagination ? loadingData(1, amountStr) : createTable(initialData);
        }
        return {getData};
    };
    const t1 = tab();
    const t2 = tab();

    t1.getData(information, {statePag: false, amountStr: 4}, document.getElementById('root_1'));
    t2.getData(information, {statePag: true, amountStr: 4}, document.getElementById('root_2'));
};


